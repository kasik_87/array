public class Array {
    public static void main(String[] args) {
        double[] arr = {54, 45, 12, 32, 98};
        for (int i = 0; i < arr.length; i++) {
            arr[i] =arr[i] * 1.1;
        }
        for (int i = arr.length-1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[j + 1]) {
                    double b = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = b;
                }
            }
        }
        for (double v : arr) {
            System.out.print(Math.round(v * Math.pow(10, 1)) / Math.pow(10, 1) + " ; ");
        }
    }
}
